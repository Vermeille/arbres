typedef struct tree234 {
    int keys[3];
    struct tree234* sons[4];
    int nbKeys;
} tree234;

tree234* makeNode(void);

void burstNode(tree234* father, int sonToBurst);

void printTree(tree234* tree);
void printTreeRec(tree234* tree, FILE* file);
void addRec(tree234* node, int value);
