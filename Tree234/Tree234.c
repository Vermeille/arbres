/*
** Tree234.c for Tree234
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 28 oct. 2011 13:47:42 CEST Guillaume "Vermeille" Sanchez
** Last update jeu. 03 nov. 2011 12:33:26 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "Tree234.h"


tree234* makeNode(void)
{
    tree234* node = malloc(sizeof (tree234));
    node->nbKeys = 0;
    node->sons[0] = NULL;
}
void add(tree234** root, int value)
{
    tree234* ghost = makeNode();
    ghost->sons[0] = *root;
    addRec(ghost, value);
    if (ghost->nbKeys != 0)
        *root = ghost;
    else
        free(ghost);
}
void addRec(tree234* node, int value)
{
    printf("on est dans %d\n", node->keys[0]);
    int i = 0;

    if (!node->sons[0])
    {
        i = node->nbKeys;
        while (i > 0 && value < node->keys[i-1])
        {
            node->keys[i] = node->keys[i-1];
            --i;
        }

        if (value == node->keys[i-1])
            return;

        node->keys[i] = value;
        node->nbKeys++;
        return;
    }

    while (i < node->nbKeys && value > node->keys[i])
        i++;

    if (value == node->keys[i])
        return;

    if (node->sons[i]->nbKeys == 3)
        burstNode(node, i);

    i = 0;
    while (i < node->nbKeys && value > node->keys[i])
        i++;

    addRec(node->sons[i], value);
}

void burstNode(tree234* father, int sonToBurst)
{
    printf("éclatement de %d, fils %d\n", father->sons[sonToBurst]->keys[0], sonToBurst);
    fflush(stdout);
    int i;
    for (i = father->nbKeys ; i > sonToBurst ; --i)
    {
        father->keys[i] = father->keys[i-1];
        father->sons[i+1] = father->sons[i];
    }
    father->nbKeys++;
    tree234* son = father->sons[sonToBurst];
    father->keys[sonToBurst] = son->keys[1];
    son->nbKeys = 1;
    father->sons[i+1] = makeNode();
    father->sons[i+1]->nbKeys = 1;
    father->sons[i+1]->keys[0] = father->sons[i]->keys[2];
    if (son->sons[0])
    {
        father->sons[i+1]->sons[0] = son->sons[2];
        father->sons[i+1]->sons[1] = son->sons[3];
    }
}

void printTree(tree234* tree)
{
    FILE* file = fopen("abr.dot", "w");
    fprintf(file, "digraph test {\n");
    printTreeRec(tree, file);
    fprintf(file,  "}\n");
    fclose(file);
}

void printTreeRec(tree234* tree, FILE* file)
{
    int i, j, k;
    for (k = 0; k <= tree->nbKeys; ++k)
    {
        fprintf(file, "\t\"");
        for (i = 0; i < tree->nbKeys ; ++i)
            fprintf(file, "%d ", tree->keys[i]);

        if (tree->sons[0])
        {
            fprintf(file, "\" -> \"");

            for (i = 0; i < tree->sons[k]->nbKeys;++i)
                fprintf(file, "%d ", tree->sons[k]->keys[i]);
        }

        fprintf(file, "\";\n");
    }

    if(tree->sons[0])
    {
        for (i = 0 ; i <= tree->nbKeys ; ++i)
        {
            printTreeRec(tree->sons[i], file);
        }
    }
}

