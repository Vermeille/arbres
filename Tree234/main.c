/*
** main.c for Tree234
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  ven. 28 oct. 2011 13:46:34 CEST Guillaume "Vermeille" Sanchez
** Last update jeu. 03 nov. 2011 12:36:20 CET Guillaume "Vermeille" Sanchez
*/

#include <stdio.h>
#include <stdlib.h>

#include "Tree234.h"

int main()
{
    tree234* root = makeNode();
    int loop = 42;

    while (loop)
    {
        scanf("%d", &loop);
        add(&root, loop);
        printTree(root);
        system("dot -Tpng abr.dot -o dot.png && eog dot.png");
    }

    return 0;
}

